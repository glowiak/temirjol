CC=		gcc
CFLAGS=	-lSDL2 -lSDL2_ttf -O3 -w
OUT=	bin/temirjol

cso:
	mkdir -p bin
	${CC} ${CFLAGS} -o ${OUT} src/main.c
