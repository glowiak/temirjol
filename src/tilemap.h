#ifndef TILEMAP_H
    #define TILEMAP_H
#endif

#define MAP_WIDTH   25
#define MAP_HEIGHT  18

/*typedef struct
{
    char* title;
    Tile tiles[MAP_WIDTH][MAP_HEIGHT];
} TileMap;

TileMap IntToTile(int in[MAP_WIDTH][MAP_HEIGHT])
{
    TileMap t;

    for (int i = 0; i < (sizeof(in)/sizeof(in[0])); i++)
    {
        for (int j = 0; j < (sizeof(in[i])/sizeof(in[i][0])); j++)
        {
            switch (in[i][j])
            {
                case TILE_TYPE_GRASS:
                {
                    t.tiles[i][j] = CreateTile(TILE_TYPE_GRASS);
                } break;
            }
        }
    }

    return t;
}*/

typedef struct
{
    int tiles[MAP_WIDTH][MAP_HEIGHT];
} WorldMap;

WorldMap CreateBlankMap()
{
    WorldMap wm;

    for (int i = 0; i < MAP_WIDTH; i++)
    {
        for (int j = 0; j < MAP_HEIGHT; j++)
        {
            wm.tiles[i][j] = 0;
        }
    }

    return wm;
}
int SaveWorldMap(WorldMap* m)
{
}
