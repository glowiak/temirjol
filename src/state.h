#ifndef STATE_H
    #define STATE_H
#endif

#define STATE_MENU  0
#define STATE_GAME  1
#define STATE_PAUSE 2

int STATE_CURRENT = STATE_MENU;
