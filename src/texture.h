#ifndef TEXTURE_H
    #define TEXTURE_H
#endif

typedef struct
{
    SDL_Texture* tex;
    int width;
    int height;
} Texture;

Texture TEX_PLAYER;
Texture TEX_GRASS;
Texture TEX_STONE;
Texture TEX_HEART;
Texture TEX_LOGO;
Texture TEX_LOGO_AR;

void loadTextures()
{
    TEX_PLAYER.width = 32; TEX_PLAYER.height = 32;
    TEX_GRASS.width = 32; TEX_GRASS.height = 32;
    TEX_STONE.width = 32; TEX_STONE.height = 32;
    TEX_HEART.width = 32; TEX_HEART.height = 32;
    TEX_LOGO.width = 400; TEX_LOGO.height = 200;
    TEX_LOGO_AR.width = TEX_LOGO.width; TEX_LOGO_AR.height = TEX_LOGO.height;

    TEX_PLAYER.tex = SDL_CreateTextureFromSurface(r, SDL_LoadBMP("resources/player.bmp"));
    TEX_GRASS.tex = SDL_CreateTextureFromSurface(r, SDL_LoadBMP("resources/grass.bmp"));
    TEX_STONE.tex = SDL_CreateTextureFromSurface(r, SDL_LoadBMP("resources/stone.bmp"));
    TEX_HEART.tex = SDL_CreateTextureFromSurface(r, SDL_LoadBMP("resources/heart.bmp"));
    TEX_LOGO.tex = SDL_CreateTextureFromSurface(r, SDL_LoadBMP("resources/logo.bmp"));
    TEX_LOGO_AR.tex = SDL_CreateTextureFromSurface(r, SDL_LoadBMP("resources/logo_ar.bmp"));
}
