#ifndef UTIL_H
    #define UTIL_H
#endif

int playerToMapX(int x)
{
    int w = 0;
    int i = 0;

    while (x > i)
    {
        w++;
        i += TILE_WIDTH;
    }

    return w - 2;
}
int playerToMapY(int y)
{
    int w = 0;
    int i = 0;

    while (y > i)
    {
        w++;
        i += TILE_HEIGHT;
    }

    return w - 1;
}
