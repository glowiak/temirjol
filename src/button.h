#ifndef BUTTON_H
    #define BUTTON_H
#endif

typedef struct
{
    int x;
    int y;
    int width;
    int height;
    int onHover;
    int clicked;
    char* caption;
} Button;

Button CreateButton(int x, int y, int width, int height, char* caption)
{
    Button b;

    b.x = x;
    b.y = y;
    b.width = width;
    b.height = height;
    b.caption = caption;

    return b;
}
void UpdateButton(Button* b)
{
    int mx = MX;
    int my = MY;
    if (mx >= b->x && mx <= b->x + b->width && my >= b->y && my <= b->y + b->height)
    {
        b->onHover = 1;
        if (MB_LEFT == 1)
        {
            b->clicked = 1;
            MB_LEFT = 0;
        } else
        {
            b->clicked = 0;
        }
    } else
    {
        b->onHover = 0;
        b->clicked = 0;
    }
}
void DrawButton(Button *b)
{
    if (b->onHover == 1)
    {
        SDL__SetColor(r, COLOR_WHITE);
    } else
    {
        SDL__SetColor(r, COLOR_DARKGRAY);
    }
    SDL__DrawRect(r, b->x, b->y, b->width, b->height);
    if (b->onHover == 1)
    {
        SDL__SetColor(r, COLOR_DARKGRAY);
    } else
    {
        SDL__SetColor(r, COLOR_WHITE);
    }
    SDL__DrawRect(r, b->x + 1, b->y + 1, b->width - 2, b->height - 2);
    if (b->onHover == 1)
    {
        SDL__SetColor(r, COLOR_WHITE);
    } else
    {
        SDL__SetColor(r, COLOR_DARKGRAY);
    }
    SDL__DrawRect(r, b->x + 2, b->y + 2, b->width - 4, b->height - 4);
    if (b->onHover == 1)
    {
        SDL__SetColor(r, COLOR_DARKGRAY);
    } else
    {
        SDL__SetColor(r, COLOR_WHITE);
    }
    SDL__DrawText(r, b->caption, b->x + 3, b->y + b->height / 4);
}
