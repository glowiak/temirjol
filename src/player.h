#ifndef PLAYER_H
    #define PLAYER_H
#endif

typedef struct
{
    int x;
    int y;
    int speed;
    int health;
    Texture tex;
    Direction direction;
} Player;

Player CreatePlayer()
{
    Player p;
    p.x = WIDTH / 2;
    p.y = HEIGHT / 2;
    p.speed = 3;
    p.health = 3;
    p.tex = TEX_PLAYER;
    p.direction.x = -1;
    p.direction.y = 0;

    return p;
}
void UpdatePlayer(Player* p)
{
    int p_mapx = playerToMapX(p->x);
    int p_mapy = playerToMapY(p->y);
    
    int block_center_x = p_mapx * TILE_WIDTH;
    int block_center_y = p_mapy * TILE_HEIGHT;
    int block_left = (p_mapx - 1) * TILE_WIDTH;
    int block_right = (p_mapx + 1) * TILE_WIDTH;
    int block_top = (p_mapy - 1) * TILE_HEIGHT;
    int block_bottom = (p_mapy + 1) * TILE_HEIGHT;
    
    printf("left player %d block %d\n", block_center_x, block_left);
    printf("right player %d block %d\n", block_center_x, block_right);
    printf("left block %d right block %d\n", wmap.tiles[p_mapx - 1][p_mapy], wmap.tiles[p_mapx + 1][p_mapy]);
    
    if (KEY_LEFT == 1)
    {
        if (block_center_x > block_left + TILE_WIDTH - 1)
        {
            if (wmap.tiles[p_mapx - 1][p_mapy] == 0)
            {
                p->x -= p->speed;
            }
        }
        p->direction.x = -1;
        p->direction.y = 0;
    }
    if (KEY_RIGHT == 1)
    {
        if (block_center_x < block_right - TILE_WIDTH + 1)
        {
            if (wmap.tiles[p_mapx + 1][p_mapy] == 0)
            {
                p->x += p->speed;
            }
        }
        p->direction.x = 1;
        p->direction.y = 0;
    }
    if (KEY_UP == 1)
    {
        if (block_center_y > block_top + TILE_HEIGHT - 1)
        {
            if (wmap.tiles[p_mapx][p_mapy - 1] == 0)
            {
                p->y -= p->speed;
            }
        }
        p->direction.x = 0;
        p->direction.y = -1;
    }
    if (KEY_DOWN == 1)
    {
        if (block_center_y < block_bottom - TILE_HEIGHT + 1)
        {
            if (wmap.tiles[p_mapx][p_mapy + 1] == 0)
            {
                p->y += p->speed;
            }
        }
        p->direction.x = 0;
        p->direction.y = 1;
    }
}
