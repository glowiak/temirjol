#ifndef COLOR_H
    #define COLOR_H
#endif

//#define TEXT_SIZE   14
#define TEXT_SIZE   10

typedef struct
{
    int r;
    int g;
    int b;
    int a;
} Color;

const Color COLOR_BLACK = { .r = 0, .g = 0, .b = 0, .a = 255, };
const Color COLOR_WHITE = { .r = 255, .g = 255, .b = 255, .a = 255, };
const Color COLOR_RED = { .r = 255, .g = 0, .b = 0, .a = 255, };
const Color COLOR_GREEN = { .r = 0, .g = 255, .b = 0, .a = 255, };
const Color COLOR_BLUE = { .r = 0, .g = 0, .b = 255, .a = 255, };
const Color COLOR_GRAY = { .r = 178, .g = 190, .b = 181, .a = 255, };
const Color COLOR_DARKGRAY = { .r = 105, .g = 105, .b = 105, .a = 255 };

TTF_Font* font;

void SDL__LoadFont(char* path)
{
    font = TTF_OpenFont(path, TEXT_SIZE);
}
void SDL__SetColor(SDL_Renderer *r, Color color)
{
    SDL_SetRenderDrawColor(r, color.r, color.g, color.b, color.a);
}
void SDL__DrawRect(SDL_Renderer* r, int x, int y, int w, int h)
{
    SDL_Rect _r =
    {
        .x = x,
        .y = y,
        .w = w,
        .h = h,
    };
    
    SDL_RenderFillRect(r, &_r);
}
void SDL__NotFillRect(SDL_Renderer* r, int x, int y, int w, int h)
{
    SDL_Rect _r = 
    {
        .x = x,
        .y = y,
        .w = w,
        .h = h,
    };
    SDL_RenderDrawRect(r, &_r);
}
void SDL__DrawTexture(SDL_Renderer* r, SDL_Texture* t, int x, int y, int w, int h)
{
    SDL_Rect rect =
    {
        .x = x,
        .y = y,
        .w = w,
        .h = h,
    };
    
    SDL_RenderCopy(r, t, NULL, &rect);
}
void SDL__DrawText(SDL_Renderer* r, char* text, int x, int y)
{
    SDL_Color c = { 0, 0, 0, 0, };
    //SDL_Color c = { 255, 255, 255, 0 };
    SDL_Surface* s = TTF_RenderText_Solid(font, text, c);
    SDL_Texture* tex = SDL_CreateTextureFromSurface(r, s);
    SDL_Rect rect;
    
    rect.x = x;
    rect.y = y;
    rect.w = s->w;
    rect.h = s->h;
    
    SDL_FreeSurface(s);
    SDL_RenderCopy(r, tex, NULL, &rect);
}
int RandomNextInt(int lower, int upper)
{
    return (rand() % (upper - lower - 1)) + lower;
}
