#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define WIDTH   800
#define HEIGHT  600
#define WFPS    60
#define SECOND  1000
#define CAPTION "Темір Жол"

SDL_Window* w;
SDL_Renderer* r;

#include "color.h"
#include "mouse.h"
#include "state.h"
#include "keys.h"
#include "texture.h"
#include "tile.h"
#include "tilemap.h"

WorldMap wmap;

#include "util.h"
#include "direction.h"
#include "player.h"
#include "button.h"

const int DRAW_INTERVAL = SECOND/WFPS;
const int Nt;

int _fps;
int realFps;
int timerr;

Player p;

Button bm_newgame;
Button bm_loadgame;
Button bm_options;
Button bm_quit;

Button bp_return;
Button bp_options;
Button bp_menu;

void init();
void update();
void updateKeys(SDL_Keysym* k);
void deupdatekeys(SDL_Keysym* k);
void draw();
void quit();
void loadMap(char* path);

void main(int argc, char* argv[])
{
    init();

    int fs;
    int ft;

    _fps = 0;
    timerr= 0;

    while (1)
    {
        fs = SDL_GetTicks();

        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            if (e.type == SDL_QUIT)
            {
                quit();
            }
            if (e.type == SDL_MOUSEBUTTONDOWN)
            {
                if (e.button.button = SDL_BUTTON_LEFT)
                {
                    MB_LEFT = 1;
                } else
                {
                    MB_LEFT = 0;
                }
                if (e.button.button = SDL_BUTTON_RIGHT)
                {
                    MB_RIGHT = 1;
                } else
                {
                    MB_RIGHT = 0;
                }
            }
            if (e.type == SDL_KEYDOWN)
            {
                updateKeys(e.key.keysym.sym);
            }
            if (e.type == SDL_KEYUP)
            {
                deupdateKeys(e.key.keysym.sym);
            }
        }
        update();
        draw();

        ft = SDL_GetTicks() - fs;

        if (DRAW_INTERVAL > ft)
        {
            SDL_Delay(DRAW_INTERVAL - ft);
        }
    }
    quit();
}
void init()
{
    MB_LEFT = 0;
    MB_RIGHT = 0;
    realFps = 0;
    SDL_Init(SDL_INIT_EVERYTHING);
    TTF_Init();
    w = SDL_CreateWindow(CAPTION, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
    r = SDL_CreateRenderer(w, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    //SDL__LoadFont("resources/font.ttf");
    SDL__LoadFont("resources/pixel.ttf");

    loadTextures();

    p = CreatePlayer();

    //loadMap("resources/map.txt");

    bm_newgame = CreateButton(WIDTH / 2 - 60, 250, 120, 25, "New Game");
    bm_loadgame = CreateButton(WIDTH / 2 - 60, 250 + 25 + 5, 120, 25, "Load Game");
    bm_options = CreateButton(WIDTH / 2 - 60, 250 + 25*2 + 5*2, 120, 25, "Options");
    bm_quit = CreateButton(WIDTH / 2 - 60, 250 + 25*3 + 5*3, 120, 25, "Quit");

    bp_return = CreateButton(WIDTH / 2 - 60, 25, 120, 25, "Return");
    bp_options = CreateButton(WIDTH / 2 - 60, 25 + 25, 120, 25, "Options");
    bp_menu = CreateButton(WIDTH / 2 - 60, 25 + 25*2, 120, 25, "Menu");

    wmap = CreateBlankMap();
}
void updateKeys(SDL_Keysym* k)
{
    if (k == SDLK_ESCAPE)
    { KEY_ESCAPE = 1; }
    if (k == SDLK_UP)
    { KEY_UP = 1; }
    if (k == SDLK_DOWN)
    { KEY_DOWN = 1; }
    if (k == SDLK_LEFT)
    { KEY_LEFT = 1; }
    if (k == SDLK_RIGHT)
    { KEY_RIGHT = 1; }
    if (k == SDLK_SPACE)
    { KEY_SPACE = 1; }
}
void deupdateKeys(SDL_Keysym* k)
{
    if (k == SDLK_ESCAPE)
    { KEY_ESCAPE = 0; }
    if (k == SDLK_UP)
    { KEY_UP = 0; }
    if (k == SDLK_DOWN)
    { KEY_DOWN = 0; }
    if (k == SDLK_LEFT)
    { KEY_LEFT = 0; }
    if (k == SDLK_RIGHT)
    { KEY_RIGHT = 0; }
    if (k == SDLK_SPACE)
    { KEY_SPACE = 0; }
}
void update()
{
    SDL_GetMouseState(&MX, &MY);
    switch (STATE_CURRENT)
    {
        case STATE_MENU:
        {
            UpdateButton(&bm_newgame);
            UpdateButton(&bm_loadgame);
            UpdateButton(&bm_options);
            UpdateButton(&bm_quit);

            if (bm_newgame.clicked == 1)
            {
                bm_newgame.clicked = 0;
                STATE_CURRENT = STATE_GAME;
            }
            if (bm_quit.clicked)
            {
                printf("Quitting...\n");
                quit();
            }

            if (KEY_ESCAPE == 1)
            {
                quit();
            }
        } break;
        case STATE_GAME:
        {
            UpdatePlayer(&p);

            if (KEY_ESCAPE == 1)
            {
                MB_LEFT = 0;
                STATE_CURRENT = STATE_PAUSE;
            }
        } break;
        case STATE_PAUSE:
        {
            UpdateButton(&bp_return);
            UpdateButton(&bp_options);
            UpdateButton(&bp_menu);

            if (bp_return.clicked == 1)
            {
                bp_return.clicked = 0;
                STATE_CURRENT = STATE_GAME;
            }
            if (bp_menu.clicked == 1)
            {
                bp_menu.clicked = 0;
                STATE_CURRENT = STATE_MENU;
            }
        } break;

        default: break;
    }

    _fps++;
    if (_fps == WFPS)
    {
        _fps = 0;
        timerr++;
    }
}
void draw()
{
    SDL__SetColor(r, COLOR_BLACK);
    SDL_RenderClear(r);
    SDL__DrawRect(r, 0, 0, WIDTH, HEIGHT);

    switch (STATE_CURRENT)
    {
        case STATE_MENU:
        {
            SDL__SetColor(r, COLOR_GRAY);
            SDL__DrawRect(r, 0, 0, WIDTH, HEIGHT);
            //SDL__SetColor(r, COLOR_WHITE);
            //SDL__DrawText(r, "ТЕМІР ЖОЛ", WIDTH / 2, 100);
            SDL__DrawTexture(r, TEX_LOGO.tex, WIDTH / 2 - TEX_LOGO.width / 2, -50, TEX_LOGO.width, TEX_LOGO.height);
            //SDL__DrawTexture(r, TEX_LOGO_AR.tex, WIDTH / 2 - TEX_LOGO_AR.width / 2, -10, TEX_LOGO_AR.width, TEX_LOGO_AR.height);
            
            DrawButton(&bm_newgame);
            DrawButton(&bm_loadgame);
            DrawButton(&bm_options);
            DrawButton(&bm_quit);
        } break;
        case STATE_GAME:
        {
            // draw map
            int base_x = 0;
            int base_y = 0;
            int tx = 0;
            int ty = 0;
            while (tx < MAP_WIDTH && ty < MAP_HEIGHT)
            {
                switch (wmap.tiles[tx][ty])
                {
                    case 0: // grass
                    {
                        SDL__DrawTexture(r, TEX_GRASS.tex, base_x, base_y, TILE_WIDTH, TILE_HEIGHT);
                    } break;
                    case 1: // stone
                    {
                        SDL__DrawTexture(r, TEX_STONE.tex, base_x, base_y, TILE_WIDTH, TILE_HEIGHT);
                    } break;

                    default: break;
                }
                base_x += TILE_WIDTH;
                tx++;
                if (tx == MAP_WIDTH)
                {
                    base_x = 0;
                    base_y += TILE_HEIGHT;

                    tx = 0;
                    ty++;
                }
            }

            // draw the player
            SDL__DrawTexture(r, p.tex.tex, p.x, p.y, p.tex.width, p.tex.height);

            base_x = WIDTH - TEX_HEART.width;

            for (int i = 0; i < p.health; i++)
            {
                SDL__DrawTexture(r, TEX_HEART.tex, base_x, 0, TEX_HEART.width, TEX_HEART.height);
                base_x -= TEX_HEART.width;
            }

            char* text = (char*)malloc(sizeof(char) * (strlen("X: ") + 4));
            sprintf(text, "X: %d", p.x);
            SDL__DrawText(r, text, 10, 10);
            text = (char*)malloc(sizeof(char) * (strlen("Y: ") + 4));
            sprintf(text, "Y: %d", p.y);
            SDL__DrawText(r, text, 10, 25);

            int mgx = playerToMapX(p.x); // MX
            int mgy = playerToMapY(p.y); // MY
            int mbx = mgx * TILE_WIDTH;
            int mby = mgy * TILE_HEIGHT;

            tx = p.direction.x;
            ty = p.direction.y;

            int nmbx = 0;
            int nmby = 0;
            switch (tx)
            {
                case -1:
                {
                    nmbx = mbx;
                    nmby = mby;
                } break;
                case 1:
                {
                    nmbx = mbx + 3 * TILE_WIDTH;
                    nmby = mby;
                } break;
                default: break;
            }
            switch (ty)
            {
                case -1:
                {
                    nmby = mby - 2 * TILE_HEIGHT;
                    nmbx = mbx + TILE_WIDTH;
                } break;
                case 1:
                {
                    nmby = mby + 2 * TILE_HEIGHT;
                    nmbx = mbx + TILE_WIDTH;
                } break;
                default: break;
            }
            mgx = playerToMapX(nmbx);
            mgy = playerToMapY(nmby);

            SDL__SetColor(r, COLOR_WHITE);
            SDL__NotFillRect(r, nmbx, nmby, TILE_WIDTH, TILE_HEIGHT);

             if (KEY_SPACE == 1)
             {
                 wmap.tiles[mgx + 2][mgy + 1] = 1;
                 MB_LEFT = 0;
             }
        } break;
        case STATE_PAUSE:
        {
            SDL__SetColor(r, COLOR_GRAY);
            SDL__DrawRect(r, 0, 0, WIDTH, HEIGHT);
            SDL__DrawText(r, "MENU", WIDTH / 2 - 20, 10);
            DrawButton(&bp_return);
            DrawButton(&bp_options);
            DrawButton(&bp_menu);
        } break;

        default: break;
    }

    SDL_RenderPresent(r);
}
void quit()
{
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    SDL_Quit();
    exit(0);
}
void loadMap(char* path)
{
    WorldMap wp = CreateBlankMap();

    FILE *fp = fopen(path, "r");
    if (fp != NULL)
    {
        for (int i = 0; i < MAP_WIDTH; i++)
        {
            for (int j = 0; j < MAP_HEIGHT; j++)
            {
                fscanf(fp, "%d", wmap.tiles[i][j]);
            }
        }
        fclose(fp);
    }

    wmap = wp;
}
void saveMap(WorldMap* m, char* path)
{
    FILE *fp = fopen(path, "w");
    if (fp != NULL)
    {
        for (int i = 0; i < MAP_WIDTH; i++)
        {
            for (int j = 0; j < MAP_HEIGHT; j++)
            {
                fprintf(fp, "%d\n", m->tiles[i][j]);
            }
        }
        fclose(fp);
    }
}
