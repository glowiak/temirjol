#ifndef TILE_H
    #define TILE_H
#endif

#define TILE_WIDTH  32
#define TILE_HEIGHT 32

#define TILE_TYPE_GRASS 0
#define TILE_TYPE_STONE 1
#define TILE_TYPE_BRICK 2

typedef struct
{
    int type;
    int breakable;
    int passable;
} Tile;

Tile CreateTile(int type)
{
    Tile t;

    switch (type)
    {
        case TILE_TYPE_GRASS:
        {
            t.type = TILE_TYPE_GRASS;
            t.breakable = 0;
            t.passable = 1;
        } break;
    }

    return t;
}
void DrawTile(Tile* t, int x, int y)
{
    switch (t->type)
    {
        case TILE_TYPE_GRASS:
        {
            SDL__DrawTexture(r, TEX_GRASS.tex, x, y, TILE_WIDTH, TILE_HEIGHT);
        } break;
    }
}
